package TempCalculator;

public class Temperature {
    double getTemperature(String isbn) {
        double change = (Double.parseDouble(isbn) * 1.8) + 32;
        return change;
    }
}
